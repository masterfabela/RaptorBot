process.env.NTBA_FIX_319 = 1;
const TelegramBot = require('node-telegram-bot-api');
const CONSTANTS = require('./constants');
const TOKEN = '336450760:AAEzojHyDAWbb_GdExpeJPOGCT4kAwNQ9QQ';
const bot = new TelegramBot(TOKEN, { polling: true });
const MESSAGES = CONSTANTS.VALID_MESSAGES;

bot.on('start', msg => {
  bot.sendMessage(msg.chat.id, 'Hola Señor');
});

bot.on('message', msg => {
  var respuestaAudio = getResponseFile(msg.text);
  var respuestaTexto = getResponseText(msg.text);
  if (respuestaAudio){
    contestarAudio(msg.chat.id, respuestaAudio);
  }
  if (respuestaTexto){
    contestarTexto(msg.chat.id, respuestaTexto);
  }
});

function getResponseFile(userMessage) {
  var response;
  const lowerCaseMessage = userMessage.toLowerCase();
  MESSAGES.forEach(function(message) {
    message.request.forEach(function(request) {
      if (lowerCaseMessage.includes(request)) {
        response = message.response;
      }
    });
  });
  return response;
}

function getResponseText(userMessage){
  var textResponse;
  const lowerCaseMessage = userMessage.toLowerCase();
  MESSAGES.forEach(function(message){
    message.request.forEach(function(request){
      if (lowerCaseMessage.includes(request)){
        textResponse = message.textResponse;
      }
    });
  });
  return textResponse;
}

function contestarAudio(id, rutaAudio) {
  if (rutaAudio){
    bot.sendVoice(id, rutaAudio);
  }
}

function contestarTexto(id, textResponse){
  if (textResponse){
    bot.sendMessage(id, response);
  }
}
